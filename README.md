# README #
Kripto para projeleri ve coin projelerinin eşsiz incelemeler sonucunda Türk kullanıcıları ile paylaşıldığı ve kripto para bilgi platformu olarak işlev görür.

Blockchain tabanlı projelerin işlevlerini ve amaçlarını incelerken bu projelere yatırım yapmayı düşünen yatırımcıları ayrıca bu tip proje geliştirmek isteyen geliştiricileri destekler.

Tamamen bağımsız bir yapıda olan proje Coin projeleri alanında eşsiz bir hizmet sunar.

[Coin Projeleri](https://wikikoin.com "Coin Projeleri")